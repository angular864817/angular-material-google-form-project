import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
export class ProfileFormComponent implements OnInit{
  profileForm!: FormGroup;
  submitted = false;
  matcher = new ErrorStateMatcher();

  gender: string[] = ['Male', 'Female'];
  hobby = ['Sports','Reading','Traveling','Other'];

  constructor(private formbuilder:FormBuilder){

    this.profileForm = this.formbuilder.group({
      name:new FormControl('',[Validators.required]),
      email:new FormControl('',[Validators.required,Validators.email]),
      gender:new FormControl('',[Validators.required]),
      hobby:new FormControl('', [Validators.required]),
      dob:new FormControl('',[Validators.required]),
      phoneno:new FormControl('',[Validators.required]),
      address:new FormControl('',[Validators.required]),
      policy:new FormControl('',[Validators.required])

    })
  }
  get f(){
    return this.profileForm.controls
  }

  ngOnInit(): void {
  }

  profileform(data:any){
    this.submitted = true
    if(this.profileForm.valid){
      console.log(data,"form")
      // this.profileForm.reset()
    }
  }

}
