import { AnsdataService } from './../services/ansdata.service';
import { __values } from 'tslib';
import { AddQuestionComponent } from './add-question/add-question.component';
import { FormArray, FormBuilder, FormControl, FormGroup, FormGroupName, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { checkbox } from '../validators/checkbox';
import {ErrorStateMatcher} from '@angular/material/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.css']
})
export class QuestionFormComponent {


  AnswerForm:FormGroup;
  formdata:any[]=[];
  matcher = new ErrorStateMatcher();
  submitted=false;


  constructor(private formbuilder:FormBuilder,
    public dialog: MatDialog,
    private ansdataService:AnsdataService,
    private router:Router){
          this.AnswerForm = this.formbuilder.group({
            about:new FormControl('',[Validators.required]),
            question: this.formbuilder.array([])
          });
    }





  openDialog(): void {
    const dialogRef = this.dialog.open(AddQuestionComponent, {
      width: '550px',
      height: '450px',

    });

    dialogRef.afterClosed().subscribe(result => {

      if(result){
        this.formdata.push(result)



        if(result.questionType == 'Checkbox V'){
         this.addquestion(result.options.map((data:any)=>data),result.question,result.fieldrequird,result.ownanswer,result.questionType)
        }

        if(result.questionType == 'Paragraph'){
          if(result.fieldrequird){
            this.questions().push(new FormGroup({
              question: new FormControl(result.question),
              options: new FormControl('', Validators.required),
              questionType: new FormControl(result.questionType)
            }))
          }
          else{
            this.questions().push(new FormGroup({
              question: new FormControl(result.question),
              options: new FormControl(''),
              questionType: new FormControl(result.questionType)
            }))
          }

        }
      }

    })


  }

  get f(){
    return this.AnswerForm.controls
  }
  questions(): FormArray {
    return this.AnswerForm.get('question') as FormArray;
  }

  newquestion(__values:any,questionname:any,field:any,ownanswer:any,questionTypes:any): FormGroup {

    let formgroup = new FormGroup({})

    __values.forEach((b: any, index: any) => {
      formgroup.addControl(b.options, new FormControl(false))
      })

      if(ownanswer){
      formgroup.addControl('customeAns', new FormControl(''))

      }

      if(field){
        formgroup.setValidators(checkbox);
        formgroup.updateValueAndValidity();
      } else {
        formgroup.setValidators([]);
        formgroup.updateValueAndValidity();
      }

    return this.formbuilder.group({
      question: questionname,
      options : this.formbuilder.array([formgroup]),
      questionType: new FormControl(questionTypes)
    });
  }

  addquestion(__values:any,questionname:any,field:any,ownanswer:any,questionType:any) {
    this.questions().push(this.newquestion(__values,questionname,field,ownanswer,questionType));
  }



  showerrcheckbox(index:any){
    return (Object.values((((this.f['question'] as FormArray).controls[index] as FormArray).controls))[1] as FormArray).controls[0].errors
  }

  showerrParagraph(index:any){
    return Object.values(((this.f['question'] as FormArray).controls[index] as FormArray).controls)[1].errors
  }



  Answerform(data:any){
    this.submitted = true;
    if(this.AnswerForm.valid){
      this.ansdataService.answerFormData.next(this.AnswerForm.value)
      this.router.navigate(['/answer-form'])
    }

  }



  }

