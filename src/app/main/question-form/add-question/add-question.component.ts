import { Component,Inject } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent {

  addQuestionForm:FormGroup
  questionType = ['Checkbox V','Paragraph'];
  matcher = new ErrorStateMatcher();
  isAddOptionShow = false

  constructor(public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data:any,
    private formbuilder:FormBuilder,
) {

      this.addQuestionForm = this.formbuilder.group({
        questionType:new FormControl('',[Validators.required]),
        question:new FormControl('',[Validators.required]),
        options:this.formbuilder.array([]),
        ownanswer:new FormControl(''),
        fieldrequird:new FormControl(''),
      })


    }

    options():FormArray{
      return this.addQuestionForm.get('options') as FormArray
    }

    NewOption():FormGroup{
      return this.formbuilder.group({
        options:new FormControl('',[Validators.required])
      })
    }
    AddOption(){
      if(this.options().length!=5){
        this.options().push(this.NewOption())
      }
      else{
        alert("max option is done")
      }

    }
    RemoveOption(id:any){
      this.options().removeAt(id)
    }

    get f(){
      return this.addQuestionForm.controls
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  addQuestionform(data:any){
    if(this.addQuestionForm.valid){
      this.dialogRef.close(this.addQuestionForm.value);
    }
  }


  buttionshow(data:any){
    if(data == 'Checkbox V'){
      this.isAddOptionShow = true
      this.options().push(this.NewOption())
      this.addQuestionForm.get('ownanswer')?.enable()
    }
    else{
      this.options().clear()
      this.isAddOptionShow = false
      this.addQuestionForm.get('options')?.disable()
      this.addQuestionForm.get('ownanswer')?.disable()
    }
  }

}
