import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainRoutingModule } from './main-routing.module';
import { ProfileFormComponent } from './profile-form/profile-form.component';
import {MatInputModule} from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import { QuestionFormComponent } from './question-form/question-form.component';
import { AddQuestionComponent } from './question-form/add-question/add-question.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDialogModule} from '@angular/material/dialog';
import { ShowanswerComponent } from './showanswer/showanswer.component';
import {MatTableModule} from '@angular/material/table';



@NgModule({
  declarations: [
    ProfileFormComponent,
    QuestionFormComponent,
    AddQuestionComponent,
    ShowanswerComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatRadioModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatButtonModule,
    FormsModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatTableModule
  ]
})
export class MainModule { }
