import { TestBed } from '@angular/core/testing';

import { AnsdataService } from './ansdata.service';

describe('AnsdataService', () => {
  let service: AnsdataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnsdataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
