import { ShowanswerComponent } from './showanswer/showanswer.component';
import { QuestionFormComponent } from './question-form/question-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileFormComponent } from './profile-form/profile-form.component';

const routes: Routes = [
  {
    path:'',
    component:ProfileFormComponent
  },
  {
    path:'question-form',
    component:QuestionFormComponent
  },
  {
    path:'answer-form',
    component:ShowanswerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
