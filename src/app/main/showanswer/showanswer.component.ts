import { map } from 'rxjs/operators';
import { AnsdataService } from './../services/ansdata.service';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { take } from 'rxjs';

@Component({
  selector: 'app-showanswer',
  templateUrl: './showanswer.component.html',
  styleUrls: ['./showanswer.component.css'],
})
export class ShowanswerComponent {
  formdata: any;

  formanslist: any[] = [];
  constructor(private ansdataService: AnsdataService) {}
  ngOnInit() {
    this.ansdataService.answerFormData
      .pipe(take(1))
      .subscribe((data: any) => (this.formdata = data));

    this.formdata.question.map((data: any) => {
      if (data.questionType == 'Checkbox V') {
        this.formanslist.push({
          question: data.question,
          optionsname: Object.keys(data.options[0]),
          optionsans: Object.values(data.options[0]),
        });
      }
      else{
        this.formanslist.push({
          question: data.question,
          optionsname: [data.options],
          optionsans:[data.options]
        });
      }

    });

  }

}
