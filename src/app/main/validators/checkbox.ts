import { AbstractControl, ValidationErrors } from "@angular/forms";

export function checkbox(control: AbstractControl): ValidationErrors | null {

  control = control.value;
  const arrofcontrol = Object.values(control).map((data:any)=>data);
  const checkboxvalarr = arrofcontrol.filter((data)=>data==true);
  const inputvaluecheck = arrofcontrol.lastIndexOf("");

  if(checkboxvalarr[0] || inputvaluecheck==-1){
    return null;
  }
  else{
    return {checkbox:true};
  }

}

